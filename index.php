<?php


require_once("vendor\autoload.php");
require_once("Autoloader.php");

use Slim\Http\Response;
use src\routes\AuthorizationMiddleware;
use src\routes\GetAircraftCodes;
use src\routes\GetAirCraftsRoute;
use src\routes\GetAirlinesDatabase;
use src\routes\GetAirportsDatabase;
use src\routes\GetCivilAirCraftRoute;
use src\routes\GetErrorCodesRoute;
use src\routes\GetFR24AirlineLogo;
use src\routes\GetFR24Countries;
use src\routes\GetFR24FlightData;
use src\routes\GetMilitaryAirCraftRoute;
use src\routes\GetSingleAirCraftsRoute;
use src\routes\LoginRoute;
use src\routes\PostCivilAirCraftRoute;
use src\routes\PostMilitaryAirCraftRoute;
use src\routes\PutCivilAirCraftRoute;
use src\routes\PutMilitaryAirCraftRoute;
use src\routes\DeleteAirCraftsRoute;
use Slim\App;
use Slim\Http\Request;
use src\routes\RegisterRoute;

new Autoloader();

$config = ['settings' => [
    'displayErrorDetails' => true,
]];
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");
$app = new App($config);
//$token = apache_request_headers()["Authorization"];
//print_r($token);
//exit();
//sleep(12);
//* Private: https://stackoverflow.com/a/12570561/12317197 *//

//if ($token) {
////    echo $token;
////    $app->getBody()->write($token);
//} else {
////    exit;
//}
//$app->add( new AuthorizationMiddleware() );
//$app->add(function ($request, $response, $next) {
//    $response->getBody()->write('BEFORE');
//    $response = $next($request, $response);
//    $response->getBody()->write('AFTER');
//
//    return $response;
//});

$app->group("/aircrafts", function () {
    $this->group("/login", function () {
        $this->post('', function ($request, $response, $args) {
            $loginRoute = new LoginRoute();
            return $loginRoute->request($request, $response, $args);
        });
    });
    $this->group("/register", function () {
        $this->post('', function ($request, $response, $args) {
            $registerRoute = new RegisterRoute();
            return $registerRoute->request($request, $response, $args);
        });
    });
    $this->group("/civil", function () {
        $this->get('', function ($request, $response, $args) {
            $getAirCivilCraftsRoute = new GetCivilAirCraftRoute();
            return $getAirCivilCraftsRoute->request($request, $response, $args);
        });
        $this->get('/version/{version-civil}', function ($request, $response, $args) {
            $getAirCivilCraftsRoute = new GetCivilAirCraftRoute();
            return $getAirCivilCraftsRoute->request($request, $response, $args);
        });
        $this->post('', function (Request $request, $response, $args) {
            $postAirCraftsRoute = new PostCivilAirCraftRoute();
            return $postAirCraftsRoute->request($request, $response, $args);
        });
        $this->put('', function ($request, $response, $args) {
            $putAirCraftsRoute = new PutCivilAirCraftRoute();
            return $putAirCraftsRoute->request($request, $response, $args);
        });
        $this->delete('/id/{id}', function ($request, $response, $args) {
            $deleteAirCraftsRoute = new DeleteAirCraftsRoute();
            return $deleteAirCraftsRoute->request($request, $response, $args);
        });
    });

    $this->group("/military", function () {
        $this->get('', function ($request, $response, $args) {
            $getAirMilitaryCraftsRoute = new GetMilitaryAirCraftRoute();
            return $getAirMilitaryCraftsRoute->request($request, $response, $args);
        });
        $this->get('/version/{version-military}', function ($request, $response, $args) {
            $getAirMilitaryCraftsRoute = new GetMilitaryAirCraftRoute();
            return $getAirMilitaryCraftsRoute->request($request, $response, $args);
        });
        $this->post('', function (Request $request, $response, $args) {
            $postAirCraftsRoute = new PostMilitaryAirCraftRoute();
            return $postAirCraftsRoute->request($request, $response, $args);
        });
        $this->put('', function ($request, $response, $args) {
            $putAirCraftsRoute = new PutMilitaryAirCraftRoute();
            return $putAirCraftsRoute->request($request, $response, $args);
        });
        $this->delete('/id/{id}', function ($request, $response, $args) {
            $deleteAirCraftsRoute = new DeleteAirCraftsRoute();
            return $deleteAirCraftsRoute->request($request, $response, $args);
        });
    });
    $this->group("/flightData", function () {
        $this->get('/detail/{id}', function ($request, $response, $args) {
            $getFR24FlightData = new GetFR24FlightData();
            return $getFR24FlightData->request($request, $response, $args);
        });
        $this->get('/detail/{id}/trail/{trail}', function ($request, $response, $args) {
            $getFR24FlightData = new GetFR24FlightData();
            return $getFR24FlightData->request($request, $response, $args);
        });
        $this->get('/aircraftCode', function ($request, $response, $args) {
            $aircraftCode = new GetAircraftCodes();
            return $aircraftCode->request($request, $response, $args);
        });
        $this->get('/airlines', function ($request, $response, $args) {
            $aircraftCode = new GetAirlinesDatabase();
            return $aircraftCode->request($request, $response, $args);
        });
        $this->get('/airports', function ($request, $response, $args) {
            $aircraftCode = new GetAirportsDatabase();
            return $aircraftCode->request($request, $response, $args);
        });
        $this->get('/airlineLogo/iata/{iata}/icao/{icao}', function ($request, $response, $args) {
            $getFR24FlightData = new GetFR24AirlineLogo();
            return $getFR24FlightData->request($request, $response, $args);
        });
        $this->get('/airlineLogo/iata//icao/{icao}', function ($request, $response, $args) {
            $getFR24FlightData = new GetFR24AirlineLogo();
            return $getFR24FlightData->request($request, $response, $args);
        });
        $this->get('/countries', function ($request, $response, $args) {
            $getFR24FlightData = new GetFR24Countries();
            return $getFR24FlightData->request($request, $response, $args);
        });
    });
    $this->get("", function (Request $request, Response $response, $args) {
        $getAirCraftsRoute = new GetAirCraftsRoute();
        return $getAirCraftsRoute->request($request, $response, $args);
    });
    $this->get("/version/{version-all}", function (Request $request, Response $response, $args) {
        $getAirCraftsRoute = new GetAirCraftsRoute();
        return $getAirCraftsRoute->request($request, $response, $args);
    });
//    $this->get("/version-military/{version-military}/version-civil/{version-civil}", function (Request $request, Response $response, $args) {
//        $getAirCraftsRoute = new GetAirCraftsRoute();
//        return $getAirCraftsRoute->request($request, $response, $args);
//    });
    $this->delete('/id/{id}', function ($request, $response, $args) {
        $deleteAirCraftsRoute = new DeleteAirCraftsRoute();
        return $deleteAirCraftsRoute->request($request, $response, $args);
    });
    $this->get('/errors', function ($request, $response, $args) {
        $ErrorsRoute = new GetErrorCodesRoute();
        return $ErrorsRoute->request($request, $response, $args);
    });
    $this->get('/aircraftCode', function ($request, $response, $args) {
        $aircraftCode = new GetAircraftCodes();
        return $aircraftCode->request($request, $response, $args);
    });
    $this->get('/single-data/{id}', function ($request, $response, $args) {
        $deleteAirCraftsRoute = new GetSingleAirCraftsRoute();
        return $deleteAirCraftsRoute->request($request, $response, $args);
    });
    $this->get('/single-data/{id}/{select}', function ($request, $response, $args) {
        $deleteAirCraftsRoute = new GetSingleAirCraftsRoute();
        return $deleteAirCraftsRoute->request($request, $response, $args);
    });
});

// Run app
$app->run();


