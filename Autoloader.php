<?php

/**
 * Autoloader.php
 * User: timo
 * Date: 19.09.19
 * Time: 13:36
 */

class Autoloader
{
    public function __construct()
    {
        spl_autoload_register(array($this, "register"));
    }

    /**
     * Start des Autoloaders, welcher require_once für alle benötigten Datein ausführt
     * @param $path string
     */
    public function register($path)
    {
        $className = str_replace("\\", DIRECTORY_SEPARATOR, $path);
        $file = __DIR__ . DIRECTORY_SEPARATOR . $className . ".php";
        if (file_exists($file)) {
            require_once($file);
        } else {
            die("Not able to load file: " . $file);
        }
    }
}