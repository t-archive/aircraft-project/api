<?php


namespace src\routes;


use src\databaseContext\AircraftDatabaseContext;
use Slim\Http\Request;
use Slim\Http\Response;
use src\factories\AircraftFactory;

class PostCivilAirCraftRoute extends Route
{
    public function request(Request $request, Response $response, $args)
    {
        $dbContext = new AircraftDatabaseContext();
        if ($dbContext->getError()) {
            $this->error = $dbContext->getError();
        }
        $parsedBody = $request->getParsedBody();
        $aircraftsNode = $parsedBody["aircraft"];
        $civilAircraft = AircraftFactory::createCivilAircraftFromJSON($aircraftsNode);
        if ($civilAircraft) {
            $resultCivilAirCrafts = $dbContext->insertCivilAircraft($civilAircraft);
        } else {
            $resultCivilAirCrafts = false;
        }

        $this->data = $resultCivilAirCrafts;
        return parent::generateResponse($response);
    }
}