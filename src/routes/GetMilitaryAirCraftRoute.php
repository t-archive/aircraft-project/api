<?php


namespace src\routes;


use Slim\Http\Request;
use Slim\Http\Response;
use src\classes\AircraftRepository;
use src\databaseContext\AircraftDatabaseContext;
use src\factories\AircraftFactory;

class GetMilitaryAirCraftRoute extends Route
{
    public function request(Request $request, Response $response, $args)
    {
        $dbContext = new AircraftDatabaseContext();
        $resultMilitaryAirCrafts = [];
        if ($dbContext->getError()) {
            $this->error = $dbContext->getError();
        }
        $result = $dbContext->getMilitaryAircraft();
        foreach ($result as $row) {
            $militaryAircraft = AircraftFactory::createMilitaryAircraftFromSQL($row);
            if ($militaryAircraft) {
                $resultMilitaryAirCrafts[] = $militaryAircraft;
            }
        }
        $militaryVersion = $dbContext->selectMilitaryVersion()[0]["version"];

        $code = 200;
        if (array_key_exists("version-military", $args) and $args["version-military"] == $militaryVersion) {
            $v_mil = false;
            $code = 204;
        } else {
            $v_mil = AircraftRepository::get($militaryVersion, $resultMilitaryAirCrafts);
        }

        $this->data = [
            "military" => $v_mil
        ];
        return parent::generateResponse($response, $code);

    }
}