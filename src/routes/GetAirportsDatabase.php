<?php


namespace src\routes;


use Slim\Http\Request;
use Slim\Http\Response;
use src\classes\AircraftCode;

class GetAirportsDatabase extends Route
{
    public function request(Request $request, Response $response, $args)
    {
        $file = file("https://raw.githubusercontent.com/jpatokal/openflights/master/data/airports.dat");
        $header = ['"id","name","city","country","iata","icao","latitude","longitude","altitude","timezone","dst", "tz-database-time-zone", "type", "source"'];
        $file = array_merge($header, $file);
        $return = [];
        $csv = array_map('str_getcsv', $file);
        array_walk($csv, function (&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });
        array_shift($csv);
        foreach ($csv as $item) {
            $dst = "Unknown";
            if ($item["dst"] == "E") {
                $dst = "Europe";
            } elseif ($item["dst"] == "A") {
                $dst = "US/Canada";
            } elseif ($item["dst"] == "S") {
                $dst = "South America";
            } elseif ($item["dst"] == "O") {
                $dst = "Australia";
            } elseif ($item["dst"] == "Z") {
                $dst = "New Zealand";
            } elseif ($item["dst"] == "N") {
                $dst = "None";
            } elseif ($item["dst"] == "U") {
                $dst = "Unknown";
            }
            if($item["iata"] != "" and $item["iata"] != "\\N" and $item["icao"] != "" and $item["icao"] != "\\N")
            $return[] = [
                "id" => intval($item["id"] + 0),
                "name" => $item["name"],
                "city" => $item["city"],
                "country" => $item["country"],
                "iata" => $item["iata"],
                "icao" => $item["icao"],
                "latitude" => $item["latitude"],
                "longitude" => $item["longitude"],
                "altitude" => $item["altitude"],
                "timezone" => $item["timezone"],
                "dst" => $item["dst"],
                "dst-text" => $dst,
                "tz-database-time-zone" => $item["tz-database-time-zone"],
                "type" => $item["type"],
                "source" => $item["source"]
            ];

        }

        $this->data = $return;
        //$this->data = ["civil" => $resultMilitaryAirCrafts];
        $code = 200;
        return parent::generateResponse($response, $code);
    }

}
