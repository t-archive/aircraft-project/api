<?php


namespace src\routes;


use src\databaseContext\AircraftDatabaseContext;
use src\classes\Error;
use Slim\Http\Request;
use Slim\Http\Response;


class DeleteAirCraftsRoute extends Route
{
    public function request(Request $request, Response $response, $args)
    {
        $data = null;
        $error = false;
        $id = $args["id"];
        if (!is_numeric($id)) {
            $this->error = new Error(409, "Invalid ID-Type");
            $error = new Error(409, "Invalid ID-Type");
        } else {
            $dbContext = new AircraftDatabaseContext();
            if ($dbContext->getError()) {
                return $response->withJson(["error" => $dbContext->getError()]);
            }
            $data = $dbContext->deleteAircraft($id);
        }
        if ($error) {
            $this->error = ["error" => json_encode($error, JSON_UNESCAPED_UNICODE)];
        } else {
            $this->data =  ["delete" => $data, "delete_id" => intval($id)];

        }

        return parent::generateResponse($response);
    }
}