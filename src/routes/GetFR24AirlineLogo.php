<?php


namespace src\routes;


use Slim\Http\Request;
use Slim\Http\Response;
use src\classes\AircraftCode;

class GetFR24AirlineLogo extends Route
{
    const noLogoImage = "https://www.flightradar24.com/static/images/data/operators/no_airline_logo.png";

    public function request(Request $request, Response $response, $args)
    {
        if (array_key_exists("iata", $args) && array_key_exists("icao", $args)) {
            $pic1 = ("https://cdn.flightradar24.com/assets/airlines/logotypes/" . $args["iata"] . "_" . $args["icao"] . ".png");
            $pic2 = ("https://www.flightradar24.com/static/images/data/operators/" . $args["icao"] . "_logo0.png");
            $file_headers = @get_headers($pic1);
            if (!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found' || $file_headers[0] == 'HTTP/1.1 403 Forbidden') {
                $file_headers = @get_headers($pic2);
                if (!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found' || $file_headers[0] == 'HTTP/1.1 403 Forbidden') {
                    $exists = self::noLogoImage;
                } else {
                    $exists = $pic2;
                }
            } else {
                $exists = $pic1;
            }
            $code = 200;
        } elseif (array_key_exists("icao", $args)) {
            $pic2 = ("https://www.flightradar24.com/static/images/data/operators/" . $args["icao"] . "_logo0.png");
            $file_headers = @get_headers($pic2);
            if (!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found' || $file_headers[0] == 'HTTP/1.1 403 Forbidden') {
                $exists = self::noLogoImage;
            } else {
                $exists = $pic2;
            }
            $code = 200;
        } else {
            $exists = self::noLogoImage;
            $code = 200;
        }

        $this->data = [
            "link" => $exists
        ];
        return parent::generateResponse($response, $code);
    }

}
