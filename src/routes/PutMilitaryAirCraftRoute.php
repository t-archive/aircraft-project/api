<?php


namespace src\routes;


use src\databaseContext\AircraftDatabaseContext;
use Slim\Http\Request;
use Slim\Http\Response;
use src\factories\AircraftFactory;

class PutMilitaryAirCraftRoute extends Route
{
    public function request(Request $request, Response $response, $args)
    {

        $dbContext = new AircraftDatabaseContext();
        if ($dbContext->getError()) {
            $this->error = $dbContext->getError();
        }

        $parsedBody = $request->getParsedBody();
        $aircraftsNode = $parsedBody["aircraft"];
        $militaryAircraft = AircraftFactory::createMilitaryAircraftFromJSON($aircraftsNode);
        if ($militaryAircraft) {
            $status = $dbContext->updateMilitaryAircraft($militaryAircraft);
            $this->data = $status;
        } else {
            $status = "Failed";
            $this->error = $status;
        }

        return parent::generateResponse($response);

    }
}