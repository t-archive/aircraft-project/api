<?php


namespace src\routes;


use Slim\Http\Request;
use Slim\Http\Response;
use src\classes\AircraftRepository;
use src\databaseContext\AircraftDatabaseContext;
use src\factories\AircraftFactory;

class GetCivilAirCraftRoute extends Route
{
    public function request(Request $request, Response $response, $args)
    {
        $dbContext = new AircraftDatabaseContext();
        if ($dbContext->getError()) {
            $this->error = $dbContext->getError();
        }
        $result = $dbContext->getCivilAircraft();
        $resultCivilAirCrafts = [];
        foreach ($result as $row) {
            $civilAircraft = AircraftFactory::createCivilAircraftFromSQL($row);
            if ($civilAircraft) {
                $resultCivilAirCrafts[] = $civilAircraft;
            }
        }

        $civilVersion = $dbContext->selectCivilVersion()[0]["version"];

        $code = 200;
        if (array_key_exists("version-civil", $args) and $args["version-civil"] == $civilVersion) {
            $v_civil = false;
            $code = 204;
            // , $args["version-civil"]]
        } else {
            $v_civil = AircraftRepository::get($civilVersion, $resultCivilAirCrafts);
        }
        $this->data = [
            "civil" => $v_civil
        ];
        //$this->data = ["civil" => $resultMilitaryAirCrafts];
        return parent::generateResponse($response,$code);
    }
}