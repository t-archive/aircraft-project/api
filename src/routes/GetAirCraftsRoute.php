<?php


namespace src\routes;



use src\classes\AircraftRepository;
use src\databaseContext\AircraftDatabaseContext;

use Slim\Http\Request;
use Slim\Http\Response;
use src\factories\AircraftFactory;

class GetAirCraftsRoute extends Route
{
    public function request(Request $request, Response $response, $args)
    {

        $dbContext = new AircraftDatabaseContext();
        if ($dbContext->getError()) {
            $this->error = $dbContext->getError();
        }
        $resultMilitaryAirCrafts = [];
        $resultCivilAirCrafts = [];
        $militaryAircrafts = $dbContext->getMilitaryAircraft();
        $civilAircrafts = $dbContext->getCivilAircraft();
        foreach ($militaryAircrafts as $militaryRow) {
            $militaryAircraftJSON = AircraftFactory::createMilitaryAircraftFromSQL($militaryRow);
            if ($militaryAircraftJSON) {
                $resultMilitaryAirCrafts[] = $militaryAircraftJSON;
            }
        }
        foreach ($civilAircrafts as $civilRow) {
            $civilAircraftJSON = AircraftFactory::createCivilAircraftFromSQL($civilRow);
            if ($civilAircraftJSON) {
                $resultCivilAirCrafts[] = $civilAircraftJSON;
            }
        }
        $militaryVersion = $dbContext->selectMilitaryVersion()[0]["version"];
        $civilVersion = $dbContext->selectCivilVersion()[0]["version"];
        $allVersion = $dbContext->selectVersion()[0]["version"];
        $code = 200;

                if (array_key_exists("version-all", $args) and $args["version-all"] == $allVersion) {
                    //$v_mil = false;
                    $code = 204;
                } else {
                    //$v_mil = AircraftRepository::get($militaryVersion, $resultMilitaryAirCrafts);
                }



                $this->data = [
                    "version" => $allVersion,
                    "military" => AircraftRepository::get($militaryVersion, $resultMilitaryAirCrafts),
                    "civil" => AircraftRepository::get($civilVersion, $resultCivilAirCrafts)
                ];






        return parent::generateResponse($response,$code);
    }
}
