<?php


namespace src\routes;



use src\databaseContext\AircraftDatabaseContext;

use Slim\Http\Request;
use Slim\Http\Response;
use src\factories\AircraftFactory;

class GetSingleAirCraftsRoute extends Route
{
    public function request(Request $request, Response $response, $args)
    {
        $dbContext = new AircraftDatabaseContext();
        if ($dbContext->getError()) {
            $this->error = $dbContext->getError();
        }
        $resultMilitaryAirCrafts = [];
        $_resultMilitaryAirCrafts = [];
        $resultCivilAirCrafts = [];
        $_resultCivilAirCrafts = [];
        $militaryAircrafts = $dbContext->getMilitaryAircraft();
        $civilAircrafts = $dbContext->getCivilAircraft();
        foreach ($militaryAircrafts as $militaryRow) {
            $militaryAircraftJSON = AircraftFactory::createMilitaryAircraftFromSQL($militaryRow);
            if ($militaryAircraftJSON) {
                $resultMilitaryAirCrafts[] = $militaryAircraftJSON;
                $_resultMilitaryAirCrafts[] = "military";
            }
        }
        foreach ($civilAircrafts as $civilRow) {
            $civilAircraftJSON = AircraftFactory::createCivilAircraftFromSQL($civilRow);
            if ($civilAircraftJSON) {

                $resultCivilAirCrafts[] = $civilAircraftJSON;
                $_resultCivilAirCrafts[] = "civil";

            }
        }

        $resultAirCrafts = array_merge($resultCivilAirCrafts, $resultMilitaryAirCrafts);
        $_resultAirCrafts = array_merge($_resultCivilAirCrafts, $_resultMilitaryAirCrafts);

        foreach (($resultAirCrafts) as $tmp_id => $json_array_tmp) {
            $json_array = json_decode(json_encode($json_array_tmp), true);
            if($json_array["id"] == $args["id"]) {
                if (array_key_exists("select", $args)) {
                    if ($args["select"] == "typ") {
                        $return = $_resultAirCrafts[$tmp_id];
                    } else {
                        $return = $json_array[$args["select"]];
                    }
                    return $return;
                    exit;
                } else {
                    $this->data = [
                        "singleAircraft" => $json_array,
                        "typ" => $_resultAirCrafts[$tmp_id]
                    ];
                }

            }
        }


        return parent::generateResponse($response);
    }
}
