<?php


namespace src\routes;


use Slim\Http\Request;
use Slim\Http\Response;
use src\keys\ErrorCodes;

class GetErrorCodesRoute extends Route
{

    public function request(Request $request, Response $response, $args)
    {
        $this->data = ["errors" => ErrorCodes::showCodesInJSON()];
        return parent::generateResponse($response);
    }

}