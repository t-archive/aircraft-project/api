<?php


namespace src\routes;


use Crew\Unsplash\User;
use src\classes\Error;
use src\classes\Login;
use src\classes\Pepper;
use src\classes\UserToken;
use src\config\UserConfig;
use src\databaseContext\AircraftDatabaseContext;
use Slim\Http\Request;
use Slim\Http\Response;
use src\databaseContext\UserDatabaseContext;
use src\factories\AircraftFactory;
use src\keys\UserDBKeys;

class LoginRoute extends Route
{
    public function request(Request $request, Response $response, $args)
    {
        $dbContext = new UserDatabaseContext();
        $code = 200;
        if ($dbContext->getError()) {
            $this->error = $dbContext->getError();
        }
        $parsedBody = $request->getParsedBody();
        if (array_key_exists("autoLogin", $parsedBody)) {
            $node_token = $parsedBody["autoLogin"]["token"];
            $data = $dbContext->getTokenByTokenDB($node_token);
            $userData = null;
            $access = false;
            if ($data) {
                $data = $data[0];
                $access = $data[UserDBKeys::EXPIRYTIME_TOKEN_DB_KEY] > time();

                $userData = $data[UserDBKeys::USER_ID_TOKEN_DB_KEY];
                if ($data && array_key_exists(UserDBKeys::USER_ID_TOKEN_DB_KEY, $data)) {
                    $loginData = $dbContext->getLogin($data[UserDBKeys::USER_ID_TOKEN_DB_KEY]);
                    $userData = [
                        UserDBKeys::ID_LOGIN_DB_KEY => $loginData[UserDBKeys::ID_LOGIN_DB_KEY],
                        UserDBKeys::EMAIL_LOGIN_DB_KEY => base64_encode($loginData[UserDBKeys::EMAIL_LOGIN_DB_KEY]),
                        UserDBKeys::FIRSTNAME_LOGIN_DB_KEY => base64_encode($loginData[UserDBKeys::FIRSTNAME_LOGIN_DB_KEY]),
                        UserDBKeys::SURNAME_LOGIN_DB_KEY => base64_encode($loginData[UserDBKeys::SURNAME_LOGIN_DB_KEY]),
                        UserDBKeys::USERNAME_LOGIN_DB_KEY => base64_encode($loginData[UserDBKeys::USERNAME_LOGIN_DB_KEY]),
                        UserDBKeys::ACTIVE_LOGIN_DB_KEY => $loginData[UserDBKeys::ACTIVE_LOGIN_DB_KEY] === 1,
                        UserDBKeys::ROLES_LOGIN_DB_KEY => explode(",", $loginData[UserDBKeys::ROLES_LOGIN_DB_KEY])
                    ];

                }
            }


            $this->data = [
                "tokenData" => $data,
                "userData" => $userData,
                "access" => $access
            ];
        } elseif (array_key_exists("renewToken", $parsedBody)) {
            $node_token = $parsedBody["renewToken"]["token"];
            $node_time = $parsedBody["renewToken"]["time"];
            $data = $dbContext->getTokenByTokenDB($node_token);

            if (is_array($data) && array_key_exists(0, $data)) {
                $data = $data[0];
            }
            if($data) {
                $time = $data[UserDBKeys::EXPIRYTIME_TOKEN_DB_KEY] + $node_time;

//            if ($node_userActive >= $time - $safeTime && $node_userActive <= $time) {
//                $time = $node_userActive + $safeTime;
//            }
                $userTokenData = new UserToken($data[UserDBKeys::USER_ID_TOKEN_DB_KEY], $data[UserDBKeys::TOKEN_TOKEN_DB_KEY], $time);
                $userTokenData->setId($data[UserDBKeys::ID_TOKEN_DB_KEY]);
                $dbContext->updateToken($userTokenData);
                $this->data = [
                    "token" => $data[UserDBKeys::TOKEN_TOKEN_DB_KEY],
                    "expirytime" => $time
                ];
            }
        } elseif (array_key_exists("getTimeData", $parsedBody)) {
            $this->data = [
                "login_time" => UserConfig::LOGIN_TIME,
                "add_login_time" => UserConfig::ADD_LOGIN_TIME
            ];
        } else {
            $node_eMail = base64_decode($parsedBody["login"]["email"]);
            $node_password = $parsedBody["login"]["password"];

            if ($node_eMail && $node_password) {
                $data = $dbContext->getLoginEP($node_eMail, Pepper::make($node_password));

                $token = md5(base64_encode($data[UserDBKeys::EMAIL_LOGIN_DB_KEY]) . time());
                $time = time() + UserConfig::LOGIN_TIME;
                $result_token = $dbContext->insertToken(new UserToken(
                    $data[UserDBKeys::ID_LOGIN_DB_KEY],
                    $token,
                    $time
                ));
                $this->data = [
                    UserDBKeys::ID_LOGIN_DB_KEY => $data[UserDBKeys::ID_LOGIN_DB_KEY],
                    UserDBKeys::EMAIL_LOGIN_DB_KEY => base64_encode($data[UserDBKeys::EMAIL_LOGIN_DB_KEY]),
                    UserDBKeys::FIRSTNAME_LOGIN_DB_KEY => base64_encode($data[UserDBKeys::FIRSTNAME_LOGIN_DB_KEY]),
                    UserDBKeys::SURNAME_LOGIN_DB_KEY => base64_encode($data[UserDBKeys::SURNAME_LOGIN_DB_KEY]),
                    UserDBKeys::USERNAME_LOGIN_DB_KEY => base64_encode($data[UserDBKeys::USERNAME_LOGIN_DB_KEY]),
                    UserDBKeys::ACTIVE_LOGIN_DB_KEY => $data[UserDBKeys::ACTIVE_LOGIN_DB_KEY] === 1,
                    UserDBKeys::ROLES_LOGIN_DB_KEY => explode(",", $data[UserDBKeys::ROLES_LOGIN_DB_KEY]),
                    "token" => $token,
                    "expirytime" => $time,
                ];

            } else {
                $code = 200;
                $this->data = [
                    "error" => "Wrong login data"
                ];
            }
        }

        return parent::generateResponse($response, $code);
    }
}
