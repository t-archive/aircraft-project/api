<?php


namespace src\routes;


use Slim\Http\Request;
use Slim\Http\Response;
use src\classes\AircraftCode;

class GetAircraftCodes extends Route
{
    public function request(Request $request, Response $response, $args)
    {
        $personalinfo = fopen("https://raw.githubusercontent.com/jpatokal/openflights/master/data/planes.dat", "r");
        $return = [];
        while(! feof($personalinfo))  {
            $result = fgets($personalinfo);
            $result = explode(",", $result);
            $result[0] = str_replace("\"", "", $result[0]);
            $result[1] = str_replace("\"", "", $result[1]);
            $result[2] = str_replace("\"", "", $result[2]);
            $result[2] = str_replace("\n", "", $result[2]);
            $result[2] = str_replace("\n", "", $result[2]);
            $result[2] = str_replace("\n", "", $result[2]);
            $return[] = array(
                "name" => $result[0],
                "iata" => $result[1],
                "icao" => $result[2]);
        }


        $this->data = $return;
        //$this->data = ["civil" => $resultMilitaryAirCrafts];
        $code = 200;
        return parent::generateResponse($response,$code);
    }

}