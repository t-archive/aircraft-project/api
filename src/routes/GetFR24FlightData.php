<?php


namespace src\routes;


use Slim\Http\Request;
use Slim\Http\Response;
use src\classes\AircraftCode;

class GetFR24FlightData extends Route
{
    private function httpGet($url)
    {
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $output=curl_exec($ch);
        curl_close($ch);
        return $output;
    }
    public function request(Request $request, Response $response, $args)
    {
//        sleep(5);
        if (array_key_exists("id", $args)) {
            if(array_key_exists("trail", $args)) {
                $data = $this->httpGet("https://data-live.flightradar24.com/clickhandler/?version=1.5&flight=".$args["id"]."&notrail=true");
            } else {
                $data = $this->httpGet("https://data-live.flightradar24.com/clickhandler/?version=1.5&flight=".$args["id"]."");
            }
            $return = json_decode($data, true);

            $code = 200;
        } else {
            $code = 204;
        }

        $this->data = $return;
        //$this->data = ["civil" => $resultMilitaryAirCrafts];

        return parent::generateResponse($response,$code);
    }

}
