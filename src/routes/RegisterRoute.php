<?php


namespace src\routes;


use src\classes\Error;
use src\classes\Login;
use src\databaseContext\AircraftDatabaseContext;
use Slim\Http\Request;
use Slim\Http\Response;
use src\databaseContext\UserDatabaseContext;
use src\factories\AircraftFactory;
use src\keys\UserDBKeys;

class RegisterRoute extends Route
{
    public function request(Request $request, Response $response, $args)
    {
        $dbContext = new UserDatabaseContext();
        if ($dbContext->getError()) {
            $this->error = $dbContext->getError();
        }
        $parsedBody = $request->getParsedBody();
        if (array_key_exists("register", $parsedBody)) {
            $node_eMail = $parsedBody["register"]["email"];
            $node_Username = $parsedBody["register"]["username"];
            $node_Firstname = $parsedBody["register"]["firstname"];
            $node_Surname = $parsedBody["register"]["surname"];
            $node_password = $parsedBody["register"]["password"];
            $code = 200;
            if ($node_eMail && $node_Username && $node_Firstname && $node_Surname && $node_password) {
                $contextLogin = new Login(base64_decode($node_eMail), base64_decode($node_Firstname), base64_decode($node_Surname), base64_decode($node_Username), $node_password);
                $data = $dbContext->getLoginEP($contextLogin->getEmail(), $contextLogin->getPasswordPepper());
                // Login mit den Daten existieren NICHT
                if ($data instanceof Error) {
                    // Neuen Anlegen
                    $this->data = $dbContext->insertLogin($contextLogin);
                } else {

                    $code = 200;
                    $this->data = new Error(409, "Account already exists");
                }
            } else {
                $code = 200;
                $this->data = new Error(100, "No Valid Login Data");
            }
        } else {
            $code = 204;
            $this->error = new Error(204);
        }

        return parent::generateResponse($response, $code);
    }
}
