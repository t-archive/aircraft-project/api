<?php


namespace src\routes;


use Slim\Http\Response;
use src\classes\Error;

abstract class Route
{
    /**
     * @var Error
     */
    protected $error;
    /**
     * @var mixed
     */
    protected $data;

    public function __construct()
    { }

    public function generateResponse(Response $response, $code_header = 200) {
        $finalJson = [];
        if ($code_header === 200) {

            if ($this->error) {
                $finalJson["error"] = $this->error;
            } else if ($this->data) {
                $finalJson["data"] = $this->data;
            } else {
                $finalJson["error"] = new Error(204);
            }
        }

        return $response->withJson($finalJson, $code_header);
    }
}