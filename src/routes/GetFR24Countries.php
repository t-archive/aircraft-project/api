<?php


namespace src\routes;


use Slim\Http\Request;
use Slim\Http\Response;
use src\classes\AircraftCode;

class GetFR24Countries extends Route
{
    public function request(Request $request, Response $response, $args)
    {
        $data = file_get_contents("https://www.flightradar24.com/mobile/countries");
        $return = json_decode($data, true);
        $code = 200;

        $this->data = $return;
        return parent::generateResponse($response, $code);
    }

}
