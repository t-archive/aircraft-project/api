<?php


namespace src\routes;


use src\databaseContext\AircraftDatabaseContext;
use Slim\Http\Request;
use Slim\Http\Response;
use src\factories\AircraftFactory;

class PutCivilAirCraftRoute extends Route
{
    public function request(Request $request, Response $response, $args)
    {

        $dbContext = new AircraftDatabaseContext();
        if ($dbContext->getError()) {
            $this->error = $dbContext->getError();
        }

        $parsedBody = $request->getParsedBody();
        $aircraftsNode = $parsedBody["aircraft"];
        $civilAircraft = AircraftFactory::createCivilAircraftFromJSON($aircraftsNode);
        if ($civilAircraft) {
            $status = $dbContext->updateCivilAircraft($civilAircraft);
        } else {
            $status = "Failed";
            $this->error = $status;
        }
        $this->data = $status;
        return parent::generateResponse($response);

    }
}