<?php


namespace src\routes;


use Slim\Http\Request;
use Slim\Http\Response;
use src\classes\AircraftCode;

class GetAirlinesDatabase extends Route
{
    public function request(Request $request, Response $response, $args)
    {
        $file = file("https://raw.githubusercontent.com/jpatokal/openflights/master/data/airlines.dat");
        $header = ['"id","name","alias","iata","icao","callsign","country","active"'];
        $file = array_merge($header, $file);
        $return = [];
//        print_r($file);
//        3320,"Lufthansa",\N,"LH","DLH","LUFTHANSA","Germany","Y"
//        324,"All Nippon Airways","ANA All Nippon Airways","NH","ANA","ALL NIPPON","Japan","Y"
//        exit();
        $csv = array_map('str_getcsv', $file);
        array_walk($csv, function (&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });
        array_shift($csv);
        foreach ($csv as $item) {
            $active = null;
            if ($item["active"] == "Y") {
                $active = true;
            } elseif ($item["active"] == "N") {
                $active = false;
            }
            $alias = $item["alias"];
            if($item["alias"] === "\\N") {
                $alias = null;
            }
            $name = $item["name"];
            if($name === "135 Airways") {
                $name = "- Unknown -";
            }
            if($item["icao"] != "-" and $item["icao"] != "" and $item["icao"] != "N/A") {
                $return[] = [
                    "id" => intval($item["id"] + 0),
                    "name" => $name,
                    "alias" => $alias,
                    "iata" => $item["iata"],
                    "icao" => $item["icao"],
                    "callsign" => $item["callsign"],
                    "country" => $item["country"],
                    "active" => $active
                ];
            }

            unset($active);
            unset($name);
            unset($alias);
        }

        $this->data = $return;
        //$this->data = ["civil" => $resultMilitaryAirCrafts];
        $code = 200;
        return parent::generateResponse($response, $code);
    }

}
