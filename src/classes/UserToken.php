<?php


namespace src\classes;


class UserToken
{
    private $id;
    private $userID;
    private $token;
    private $expirytime;


    /**
     * UserToken constructor.
     * @param int $userID
     * @param string $token
     * @param int $expirytime
     */
    public function __construct(
        int $userID,
        string $token,
        int $expirytime
    )
    {
        $this->userID = $userID;
        $this->token = $token;
        $this->expirytime = $expirytime;

    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "userID" => $this->userID,
            "token" => $this->token,
            "expirytime" => $this->expirytime
        ];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserID(): int
    {
        return $this->userID;
    }

    /**
     * @param int $userID
     */
    public function setUserID(int $userID): void
    {
        $this->userID = $userID;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return int
     */
    public function getExpirytime(): int
    {
        return $this->expirytime;
    }

    /**
     * @param int $expirytime
     */
    public function setExpirytime(int $expirytime): void
    {
        $this->expirytime = $expirytime;
    }
}
