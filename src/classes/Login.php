<?php


namespace src\classes;


class Login
{
    private $id;
    private $email;
    private $firstname;
    private $surname;
    private $username;
    private $password;
    private $active;


    /**
     * Login constructor.
     * @param string $email
     * @param string $firstname
     * @param string $surname
     * @param string $username
     * @param string $password
     * @param bool $active
     */
    public function __construct(
        string $email,
        string $firstname,
        string $surname,
        string $username,
        string $password,
        bool $active = true
    )
    {
        $this->email = $email;
        $this->firstname = $firstname;
        $this->surname = $surname;
        $this->username = $username;
        $this->password = $password;
        $this->active = $active;

    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "email" => $this->email,
            "firstname" => $this->firstname,
            "surname" => $this->surname,
            "username" => $this->username,
            "password" => $this->password,
            "active" => $this->active
        ];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname(string $firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
    /**
     * @return string
     */
    public function getPasswordPepper(): string
    {
        return Pepper::make($this->password);
    }
    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }


}
