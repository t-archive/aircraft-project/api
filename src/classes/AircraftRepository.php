<?php


namespace src\classes;

class AircraftRepository
{
    /**
     * @var string $version
     */
    private $version;
    /**
     * @var Aircraft[]
     */
    private $aircrafts;

    public function __construct(string $version, string $aircrafts)
    {
        $this->version = $version;
        $this->aircrafts = $aircrafts;
    }

    public static function get( $version, $aircrafts) {

        return ["version" => $version, "aircrafts" => $aircrafts];
    }
}
