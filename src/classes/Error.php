<?php


namespace src\classes;


use JsonSerializable;
use src\keys\ErrorCodes;

class Error implements JsonSerializable
{
    private $errorCode;
    private $errorMessage;

    public function __construct(int $code = 200, string $message = null)
    {
        $this->errorCode = $code;
        if (!isset($message) && $message == "") {
            $this->errorMessage = ErrorCodes::httpErrorCode($code);
        } else {
            $this->errorMessage = $message;
        }

    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            "code" => $this->errorCode,
            "message" => $this->errorMessage
        ];
    }
}