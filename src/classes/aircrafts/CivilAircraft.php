<?php


namespace src\classes\aircrafts;

use JsonSerializable;
use src\classes\Aircraft;


class CivilAircraft extends Aircraft implements JsonSerializable
{
    /**
     * @var bool
     */
    private $gateLicense;

    /**
     * @return bool
     */
    public function getGateLicense(): bool
    {
        return $this->gateLicense;
    }

    /**
     * @param bool $gateLicense
     */
    public function setGateLicense(bool $gateLicense): void
    {
        $this->gateLicense = $gateLicense;
    }


    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(), [
            "gateLicense" => $this->gateLicense
        ]);
    }

}
