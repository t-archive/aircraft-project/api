<?php


namespace src\classes\aircrafts;

use JsonSerializable;
use src\classes\Aircraft;


class MilitaryAircraft extends Aircraft implements JsonSerializable
{
    /**
     * @var bool
     */
    private $flare;

    /**
     * @var bool
     */
    private $airRefueling;

    /**
     * @return bool
     */
    public function getAirRefueling()
    {
        return $this->airRefueling;
    }

    /**
     * @param bool $airRefueling
     */
    public function setAirRefueling(bool $airRefueling): void
    {

        $this->airRefueling = $airRefueling;
    }

    /**
     * @return bool
     */
    public function getFlare()
    {
        return $this->flare;
    }

    /**
     * @param bool $flare
     */
    public function setFlare(bool $flare): void
    {
        $this->flare = $flare;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(), [
            "flare" => $this->flare,
            "airRefueling" => $this->airRefueling
        ]);
    }

}
