<?php


namespace src\classes;

use JsonSerializable;

abstract class Aircraft implements JsonSerializable
{
    private $id;
    private $name;
    private $distance;
    private $seatCount;
    private $maxSeatCount;
    private $length;
    private $wingSpan;
    private $height;
    private $consumption;


    /**
     * Aircraft constructor.
     * @param string $name
     * @param int $distance
     * @param int $seatCount
     * @param int $maxSeatCount
     * @param float $length
     * @param float $wingSpan
     * @param float $height
     * @param float $consumption
     */
    public function __construct(
        string $name,
        int $distance,
        int $seatCount,
        int $maxSeatCount,
        float $length,
        float $wingSpan,
        float $height,
        float $consumption
    )
    {

        $this->name = $name;
        $this->distance = $distance;
        $this->seatCount = $seatCount;
        $this->maxSeatCount = $maxSeatCount;
        $this->length = $length;
        $this->wingSpan = $wingSpan;
        $this->height = $height;
        $this->consumption = $consumption;

    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): array
    {
        return [
            "id" => intval($this->id),
            "name" => $this->name,
            "distance" => intval($this->distance),
            "seatCount" => intval($this->seatCount),
            "maxSeatCount" => intval($this->maxSeatCount),
            "length" => floatval($this->length),
            "wingSpan" => floatval($this->wingSpan),
            "height" => floatval($this->height),
            "consumption" => floatval($this->consumption)
        ];
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * @return mixed
     */
    public function getSeatCount()
    {
        return $this->seatCount;
    }

    /**
     * @return mixed
     */
    public function getMaxSeatCount()
    {
        return $this->maxSeatCount;
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @return mixed
     */
    public function getWingSpan()
    {
        return $this->wingSpan;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @return mixed
*/
    public function getConsumption()
    {
        return $this->consumption;
    }
}