<?php


namespace src\keys;


abstract class AircraftKeys
{
    const ID_DB_KEY = "id";
    const NAME_DB_KEY = "name";
    const DISTANCE_DB_KEY = "distance";
    const SEATCOUNT_DB_KEY = "seatCount";
    const MAXSEATCOUNT_DB_KEY = "maxSeatCount";
    const LENGTH_DB_KEY = "length";
    const WINGSPAN_DB_KEY = "wingSpan";
    const HEIGHT_DB_KEY = "height";
    const CONSUMPTION_DB_KEY = "consumption";
    const AIR_REFUELING_DB_KEY = "air_refueling";
    const FLARE_DB_KEY = "flare";
    const GATE_LICENSE_DB_KEY = "gate_license";

    const SUBID_FROM_CIVIL_AIRCRAFT_DB_KEY = "subid";
    const SUBID_FROM_MILITARY_AIRCRAFT_DB_KEY = "subid";
    const ID_AIRCRAFT_FROM_CIVIL_AIRCRAFT_DB_KEY = "id_aircraft";
    const ID_AIRCRAFT_FROM_MILITARY_AIRCRAFT_DB_KEY = "id_aircraft";

    const ID_JSON_KEY = "id";
    const NAME_JSON_KEY = "name";
    const DISTANCE_JSON_KEY = "distance";
    const SEATCOUNT_JSON_KEY = "seatCount";
    const MAXSEATCOUNT_JSON_KEY = "maxSeatCount";
    const LENGTH_JSON_KEY = "length";
    const WINGSPAN_JSON_KEY = "wingSpan";
    const HEIGHT_JSON_KEY = "height";
    const CONSUMPTION_JSON_KEY = "consumption";
    const AIR_REFUELING_JSON_KEY = "airRefueling";
    const FLARE_JSON_KEY = "flare";
    const GATE_LICENSE_JSON_KEY = "gateLicense";

}
