<?php


namespace src\keys;


class UserDBKeys
{
    const ID_LOGIN_DB_KEY = "id";
    const EMAIL_LOGIN_DB_KEY = "email";
    const FIRSTNAME_LOGIN_DB_KEY = "firstname";
    const SURNAME_LOGIN_DB_KEY = "surname";
    const USERNAME_LOGIN_DB_KEY = "username";
    const PASSWORD_LOGIN_DB_KEY = "password";
    const ACTIVE_LOGIN_DB_KEY = "active";
    const ROLES_LOGIN_DB_KEY = "roles";
    const ID_TOKEN_DB_KEY = "id";
    const USER_ID_TOKEN_DB_KEY = "userID";
    const TOKEN_TOKEN_DB_KEY = "token";
    const EXPIRYTIME_TOKEN_DB_KEY = "expirytime";

}
