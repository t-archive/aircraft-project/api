<?php


namespace src\databaseContext;


use src\classes\Error;
use src\classes\Login;
use src\classes\Pepper;
use src\classes\UserToken;
use src\config\DBConfig;

class UserDatabaseContext
{
    const UPDATE_SQL = "UPDATE login SET email = ?, firstname = ?, surname = ?, username = ?,  password = ? WHERE id=?;";
    const INSERT_SQL = "INSERT INTO login (email, firstname, surname, username, password) VALUES (?,?,?,?,?)";
    const INSERT_LOGIN_TO_USER_RANK_SQL = "INSERT INTO login_to_userrank (login_id,rank_id) VALUES (?,?)";
    const DELETE_SQL = "DELETE FROM login WHERE id=?";
    const SELECT_SQL = "
    SELECT l.id,
		l.email,
        l.firstname,
        l.surname,
        l.username,
        l.password,
        l.active,
        GROUP_CONCAT(ur.name) AS `roles` 
FROM `login` l
LEFT JOIN login_to_userrank ltu
		ON ltu.login_id = l.id
LEFT JOIN userrank ur
		ON ur.userrank_id = ltu.rank_id
GROUP BY l.id
    ";
    const SELECT_EP_SQL = "SELECT l.id,
		l.email,
        l.firstname,
        l.surname,
        l.username,
        l.password,
        l.active,
        GROUP_CONCAT(ur.name) AS `roles` 
FROM `login` l
LEFT JOIN login_to_userrank ltu
		ON ltu.login_id = l.id
LEFT JOIN userrank ur
		ON ur.userrank_id = ltu.rank_id
WHERE l.email=? && l.password=?
GROUP BY l.id";
    const SELECT_ID_SQL = "
    SELECT l.id,
		l.email,
        l.firstname,
        l.surname,
        l.username,
        l.password,
        l.active,
        GROUP_CONCAT(ur.name) AS `roles` 
FROM `login` l
LEFT JOIN login_to_userrank ltu
		ON ltu.login_id = l.id
LEFT JOIN userrank ur
		ON ur.userrank_id = ltu.rank_id
		 WHERE l.id=?
GROUP BY l.id
    ";
    const TOKEN_UPDATE_SQL = "UPDATE user_token SET userID = ?, token = ?, expirytime = ? WHERE id=?;";
    const TOKEN_INSERT_SQL = "INSERT INTO user_token (userID, token, expirytime) VALUES (?,?,?)";
    const TOKEN_DELETE_SQL = "DELETE FROM user_token WHERE id=?";
    const TOKEN_DELETE_BY_TOKEN_SQL = "DELETE FROM user_token WHERE token=?";
    const TOKEN_SELECT_SQL = "SELECT * FROM user_token";
    const TOKEN_BY_ID_SELECT_SQL = "SELECT * FROM user_token WHERE id=?";
    const TOKEN_BY_TOKEN_SELECT_SQL = "SELECT * FROM user_token WHERE token=?";

    private $error;
    private $connection;

    public function __construct()
    {
        $this->connection = mysqli_connect(
            DBConfig::DB_HOST,
            DBConfig::DB_USER,
            DBConfig::DB_PASS,
            DBConfig::DB_DATABASE
        );
        if (mysqli_connect_errno()) {
            $this->error = mysqli_connect_error();
        }
    }

    public function updateLogin(Login $login)
    {
        $successful = false;
        $id = $login->getId();
        $email = $login->getEmail();
        $firstname = $login->getFirstname();
        $surname = $login->getSurname();
        $username = $login->getUsername();
        $password = $login->getPasswordPepper();
        $stmt = $this->connection->prepare(self::UPDATE_SQL);
        if (mysqli_stmt_bind_param($stmt, "sssssi",
            $email,
            $firstname,
            $surname,
            $username,
            $password,
            $id
        )) {
            $successful = mysqli_stmt_execute($stmt);
        }

        if (!$successful) {
            // Log error to file
        }
        $insert_id = $id;

        if (!$successful) {
            // Log error to file
        }
        return ["success" => $successful];
    }

    public function insertLogin(Login $login)
    {
        $successful = false;
        $email = $login->getEmail();
        $firstname = $login->getFirstname();
        $surname = $login->getSurname();
        $username = $login->getUsername();
        $password = $login->getPasswordPepper();


        $stmt = $this->connection->prepare(self::INSERT_SQL);
        if (mysqli_stmt_bind_param($stmt, "sssss",
            $email,
            $firstname,
            $surname,
            $username,
            $password
        )) {
            $successful = mysqli_stmt_execute($stmt);
        }
        $insert_id = $stmt->insert_id;

        if (!$successful) {
            // Log error to file
        }
        $this->insertLoginToUserRank($insert_id);
        return ["success" => $successful, "id" => $insert_id];

    }
    public function insertLoginToUserRank($loginID)
    {
        $successful = false;
        $rankUser = 2;
        $stmt = $this->connection->prepare(self::INSERT_LOGIN_TO_USER_RANK_SQL);
        if (mysqli_stmt_bind_param($stmt, "ii",
            $loginID, $rankUser
        )) {
            $successful = mysqli_stmt_execute($stmt);
        }

        if (!$successful) {
            // Log error to file
        }
        return $successful;

    }
    public function deleteLogin(int $loginID)
    {
        $successful = false;
        $stmt = $this->connection->prepare(self::DELETE_SQL);
        if (mysqli_stmt_bind_param($stmt, "i",
            $loginID
        )) {
            $successful = mysqli_stmt_execute($stmt) && ($this->connection->affected_rows > 0);
        }
        return $successful;
    }

    public function getLoginEP($email, $pepperPassword) {
//        var_dump([$email, $pepperPassword]);exit;
        $stmt = $this->connection->prepare(self::SELECT_EP_SQL);
        $result = false;
        $successful = false;

        if (mysqli_stmt_bind_param($stmt, "ss",
            $email, $pepperPassword
        )) {
            $successful = mysqli_stmt_execute($stmt);
        }
        if ($successful) {
            $selectResult = mysqli_stmt_get_result($stmt);
            $result = mysqli_fetch_all($selectResult, MYSQLI_ASSOC);
            if(is_array($result) && count($result) > 0) {
                $result = $result[0];
            } else {
                $successful = false;
            }
        }
        if (!$successful) {
            // Log error to file
            $result = new Error(409);
        }
        return $result;
    }
    public function getLogin($id)
    {
//        var_dump([$email, $pepperPassword]);exit;
        $stmt = $this->connection->prepare(self::SELECT_ID_SQL);
        $result = false;
        $successful = false;

        if (mysqli_stmt_bind_param($stmt, "i",
            $id
        )) {
            $successful = mysqli_stmt_execute($stmt);
        }
        if ($successful) {
            $selectResult = mysqli_stmt_get_result($stmt);
            $result = mysqli_fetch_all($selectResult, MYSQLI_ASSOC);
            if(is_array($result) && count($result) > 0) {
                $result = $result[0];
            } else {
                $successful = false;
            }
        }
        if (!$successful) {
            // Log error to file
            $result = new Error(409);
        }
        return $result;

    }
    public function getLoginArray() {
        $resultLogin = false;
        $stmt = $this->connection->prepare(self::SELECT_SQL);
        if ($stmt) {

        } else {
            exit();
        }
        $successful = mysqli_stmt_execute($stmt);
        if ($successful) {
            $selectResult = mysqli_stmt_get_result($stmt);
            $resultLogin = mysqli_fetch_all($selectResult, MYSQLI_ASSOC);
        }
        if (!$successful) {
            // Log error to file
        }
        return $resultLogin;
    }

    public function updateToken(UserToken $userToken)
    {
        $successful = false;
        $id = $userToken->getId();
        $userID = $userToken->getUserID();
        $token = $userToken->getToken();
        $expiryTime = $userToken->getExpirytime();

        $stmt = $this->connection->prepare(self::TOKEN_UPDATE_SQL);
        if (mysqli_stmt_bind_param($stmt, "isii",
            $userID,
            $token,
            $expiryTime,
            $id
        )) {
            $successful = mysqli_stmt_execute($stmt);
        }

        if (!$successful) {
            // Log error to file
        }
        $insert_id = $id;

        if (!$successful) {
            // Log error to file
        }
        return ["success" => $successful];
    }

    public function insertToken(UserToken $login)
    {
        $successful = false;
        $id = $login->getId();
        $userID = $login->getUserID();
        $token = $login->getToken();
        $expiryTime = $login->getExpirytime();


        $stmt = $this->connection->prepare(self::TOKEN_INSERT_SQL);
        if (mysqli_stmt_bind_param($stmt, "isi",
            $userID,
            $token,
            $expiryTime
        )) {
            $successful = mysqli_stmt_execute($stmt);
        }
        $insert_id = $stmt->insert_id;

        if (!$successful) {
            // Log error to file
        }
        return ["success" => $successful, "id" => $insert_id];

    }

    public function deleteToken(int $tokenID)
    {
        $successful = false;
        $stmt = $this->connection->prepare(self::TOKEN_DELETE_SQL);
        if (mysqli_stmt_bind_param($stmt, "i",
            $tokenID
        )) {
            $successful = mysqli_stmt_execute($stmt) && ($this->connection->affected_rows > 0);
        }
        return $successful;
    }

    public function deleteTokenByToken(string $token)
    {
        $successful = false;
        $stmt = $this->connection->prepare(self::TOKEN_DELETE_BY_TOKEN_SQL);
        if (mysqli_stmt_bind_param($stmt, "s",
            $token
        )) {
            $successful = mysqli_stmt_execute($stmt) && ($this->connection->affected_rows > 0);
        }
        return $successful;
    }

    public function getTokenByTokenDB($token)
    {
        $stmt = $this->connection->prepare(self::TOKEN_BY_TOKEN_SELECT_SQL);
        $resultVersion = false;
        $successful = false;
        if (mysqli_stmt_bind_param($stmt, "s",
            $token
        )) {
            $successful = mysqli_stmt_execute($stmt);
        }
        if ($successful) {
            $selectResult = mysqli_stmt_get_result($stmt);
            $resultVersion = mysqli_fetch_all($selectResult, MYSQLI_ASSOC);
        }
        if (!$successful) {
            // Log error to file
            $resultVersion = new Error(409);
        }
        return $resultVersion;
    }

    public function getToken()
    {
        $resultLogin = false;
        $stmt = $this->connection->prepare(self::TOKEN_SELECT_SQL);
        if ($stmt) {

        } else {
            exit();
        }
        $successful = mysqli_stmt_execute($stmt);
        if ($successful) {
            $selectResult = mysqli_stmt_get_result($stmt);
            $resultLogin = mysqli_fetch_all($selectResult, MYSQLI_ASSOC);
        }
        if (!$successful) {
            // Log error to file
        }
        return $resultLogin;
    }


    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }
}
