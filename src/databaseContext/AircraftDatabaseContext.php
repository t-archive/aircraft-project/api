<?php


namespace src\databaseContext;


use src\classes\Error;
use src\config\DBConfig;
use src\classes\aircrafts\CivilAircraft;
use src\classes\aircrafts\MilitaryAircraft;


class AircraftDatabaseContext
{
    const UPDATE_SQL = "UPDATE aircraft SET name = ?, distance = ?, seatCount = ?, maxSeatCount = ?, length = ?, wingSpan = ?, height = ?, consumption = ? WHERE id=?;";
    const INSERT_SQL = "INSERT INTO aircraft (name, distance, seatCount, maxSeatCount, length, wingSpan, height,consumption) VALUES (?,?,?,?,?,?,?,?)";
    const DELETE_SQL = "DELETE FROM aircraft WHERE id=?";
    const SELECT_SQL = "SELECT * FROM aircraft";

    const SELECT_MILITARY_AIRCRAFT = "SELECT aircraft.*, military_aircraft.* FROM aircraft, military_aircraft WHERE aircraft.id = military_aircraft.id_aircraft;";
    const SELECT_CIVIL_AIRCRAFT = "SELECT aircraft.*, civil_aircraft.* FROM aircraft, civil_aircraft WHERE aircraft.id = civil_aircraft.id_aircraft;";
    const INSERT_CIVIL_SQL = "INSERT INTO civil_aircraft (id_aircraft, gate_license) VALUES (?,?)";
    const INSERT_MILITARY_SQL = "INSERT INTO military_aircraft (id_aircraft, flare, air_refueling) VALUES (?,?,?)";
    const UPDATE_CIVIL_SQL = "UPDATE civil_aircraft SET gate_license = ? WHERE id_aircraft=?;";
    const UPDATE_MILITARY_SQL = "UPDATE military_aircraft SET flare = ?, air_refueling = ? WHERE id_aircraft=?;";

    const SELECT_VERSION = "SELECT * FROM versions WHERE data_key = ?";

    //const UPDATE_SQL = "CALL sp_update_aircraft(?,?,?,?,?);";

    private $error;
    private $connection;

    public function __construct()
    {
        $this->connection = mysqli_connect(
            DBConfig::DB_HOST,
            DBConfig::DB_USER,
            DBConfig::DB_PASS,
            DBConfig::DB_DATABASE
        );
        if (mysqli_connect_errno()) {
            $this->error = mysqli_connect_error();
        }
    }


    /**
     * @return array|bool|Error|null
     */
    public function selectCivilVersion()
    {
        $stmt = $this->connection->prepare(self::SELECT_VERSION);
        $data_typ = "civil_aircraft";
        $resultVersion = false;
        $successful = false;
        if (mysqli_stmt_bind_param($stmt, "s",
            $data_typ
        )) {
            $successful = mysqli_stmt_execute($stmt);
        }
        if ($successful) {
            $selectResult = mysqli_stmt_get_result($stmt);
            $resultVersion = mysqli_fetch_all($selectResult, MYSQLI_ASSOC);
        }
        if (!$successful) {
            // Log error to file
            $resultVersion = new Error(409);
        }
        return $resultVersion;
    }

    public function selectMilitaryVersion()
    {
        $stmt = $this->connection->prepare(self::SELECT_VERSION);
        $data_typ = "military_aircraft";
        $resultVersion = false;
        $successful = false;
        if (mysqli_stmt_bind_param($stmt, "s",
            $data_typ
        )) {
            $successful = mysqli_stmt_execute($stmt);
        }
        if ($successful) {
            $selectResult = mysqli_stmt_get_result($stmt);
            $resultVersion = mysqli_fetch_all($selectResult, MYSQLI_ASSOC);
        }
        if (!$successful) {
            // Log error to file
            $resultVersion = new Error(409);
        }
        return $resultVersion;
    }

    public function selectVersion()
    {
        $stmt = $this->connection->prepare(self::SELECT_VERSION);
        $data_typ = "aircraft";
        $resultVersion = false;
        $successful = false;
        if (mysqli_stmt_bind_param($stmt, "s",
            $data_typ
        )) {
            $successful = mysqli_stmt_execute($stmt);
        }
        if ($successful) {
            $selectResult = mysqli_stmt_get_result($stmt);
            $resultVersion = mysqli_fetch_all($selectResult, MYSQLI_ASSOC);
        }
        if (!$successful) {
            // Log error to file
            $resultVersion = new Error(409);
        }
        return $resultVersion;
    }


    /**
     * @param MilitaryAircraft $militaryAircraft
     * @return array
     */
    public function updateMilitaryAircraft(MilitaryAircraft $militaryAircraft)
    {
        $successful = false;
        $successful_mildb = false;
        $name = $militaryAircraft->getName();
        $distance = $militaryAircraft->getDistance();
        $seatCount = $militaryAircraft->getSeatCount();
        $maxSeatCount = $militaryAircraft->getMaxSeatCount();
        $length = $militaryAircraft->getLength();
        $wingSpan = $militaryAircraft->getWingSpan();
        $height = $militaryAircraft->getHeight();
        $consumption = $militaryAircraft->getConsumption();
        $id = $militaryAircraft->getId();
        $flare = $militaryAircraft->getFlare();
        $airRefueling = $militaryAircraft->getAirRefueling();

        $stmt = $this->connection->prepare(self::UPDATE_SQL);
        if (mysqli_stmt_bind_param($stmt, "siiissssi",
            $name,
            $distance,
            $seatCount,
            $maxSeatCount,
            $length,
            $wingSpan,
            $height,
            $consumption,
            $id
        )) {
            $successful = mysqli_stmt_execute($stmt);
        }

        if (!$successful) {
            // Log error to file
        }
        $insert_id = $id;

        if (!$successful) {
            // Log error to file
        }
        $stmt_mildb = $this->connection->prepare(self::UPDATE_MILITARY_SQL);
        if (mysqli_stmt_bind_param($stmt_mildb, "iii",
            $flare,
            $airRefueling,
            $insert_id
        )) {
            $successful_mildb = mysqli_stmt_execute($stmt_mildb);
        }
        if (!$successful_mildb) {
            // Log error to file
        }
        return ["success" => $successful && $successful_mildb];
    }

    /**
     * @param CivilAircraft $civilAircraft
     * @return array
     */
    public function updateCivilAircraft(CivilAircraft $civilAircraft)
    {
        $successful = false;
        $successful_cildb = false;
        $name = $civilAircraft->getName();
        $distance = $civilAircraft->getDistance();
        $seatCount = $civilAircraft->getSeatCount();
        $maxSeatCount = $civilAircraft->getMaxSeatCount();
        $length = $civilAircraft->getLength();
        $wingSpan = $civilAircraft->getWingSpan();
        $height = $civilAircraft->getHeight();
        $consumption = $civilAircraft->getConsumption();
        $id = $civilAircraft->getId();
        $gateLicense = $civilAircraft->getGateLicense();

        $stmt = $this->connection->prepare(self::UPDATE_SQL);
        if (mysqli_stmt_bind_param($stmt, "siiissssi",
            $name,
            $distance,
            $seatCount,
            $maxSeatCount,
            $length,
            $wingSpan,
            $height,
            $consumption,
            $id
        )) {
            $successful = mysqli_stmt_execute($stmt);
        }

        if (!$successful) {
            // Log error to file
        }
        $insert_id = $id;

        if (!$successful) {
            // Log error to file
        }
        $stmt_cildb = $this->connection->prepare(self::UPDATE_CIVIL_SQL);
        if (mysqli_stmt_bind_param($stmt_cildb, "ii",
            $gateLicense,
            $insert_id
        )) {
            $successful_cildb = mysqli_stmt_execute($stmt_cildb);
        }
        if (!$successful_cildb) {
            // Log error to file
        }
        return ["success" => $successful && $successful_cildb];
    }

    /**
     * @param MilitaryAircraft $militaryAircraft
     * @return array
     */
    public function insertMilitaryAircraft(MilitaryAircraft $militaryAircraft)
    {
        $successful = false;
        $successful_mildb = false;
        $name = $militaryAircraft->getName();
        $distance = $militaryAircraft->getDistance();
        $seatCount = $militaryAircraft->getSeatCount();
        $maxSeatCount = $militaryAircraft->getMaxSeatCount();
        $length = $militaryAircraft->getLength();
        $wingSpan = $militaryAircraft->getWingSpan();
        $height = $militaryAircraft->getHeight();
        $consumption = $militaryAircraft->getConsumption();
        $airRefueling = $militaryAircraft->getAirRefueling();
        $flare = $militaryAircraft->getFlare();


        $stmt = $this->connection->prepare(self::INSERT_SQL);
        if (mysqli_stmt_bind_param($stmt, "siiissss",
            $name,
            $distance,
            $seatCount,
            $maxSeatCount,
            $length,
            $wingSpan,
            $height,
            $consumption
        )) {
            $successful = mysqli_stmt_execute($stmt);
        }
        $insert_id = $stmt->insert_id;

        if (!$successful) {
            // Log error to file
        }
        $stmt_mildb = $this->connection->prepare(self::INSERT_MILITARY_SQL);
        if (mysqli_stmt_bind_param($stmt_mildb, "iii",
            $insert_id,
            $flare,
            $airRefueling
        )) {
            $successful_mildb = mysqli_stmt_execute($stmt_mildb);
        }
        if (!$successful_mildb) {
            // Log error to file
        }
        return ["success" => true, "id" => $insert_id];

    }

    /**
     * @param CivilAircraft $civilAircraft
     * @return array
     */
    public function insertCivilAircraft(CivilAircraft $civilAircraft)
    {
        $successful = false;
        $successful_cildb = false;
        $name = $civilAircraft->getName();
        $distance = $civilAircraft->getDistance();
        $seatCount = $civilAircraft->getSeatCount();
        $maxSeatCount = $civilAircraft->getMaxSeatCount();
        $length = $civilAircraft->getLength();
        $wingSpan = $civilAircraft->getWingSpan();
        $height = $civilAircraft->getHeight();
        $consumption = $civilAircraft->getConsumption();
        $gateLicense = $civilAircraft->getGateLicense();


        $stmt = $this->connection->prepare(self::INSERT_SQL);
        if (mysqli_stmt_bind_param($stmt, "siiissss",
            $name,
            $distance,
            $seatCount,
            $maxSeatCount,
            $length,
            $wingSpan,
            $height,
            $consumption
        )) {
            $successful = mysqli_stmt_execute($stmt);
        }
        $insert_id = $stmt->insert_id;

        if (!$successful) {
            // Log error to file
        }
        $stmt_cildb = $this->connection->prepare(self::INSERT_CIVIL_SQL);
        if (mysqli_stmt_bind_param($stmt_cildb, "ii",
            $insert_id,
            $gateLicense
        )) {
            $successful_cildb = mysqli_stmt_execute($stmt_cildb);
        }
        if (!$successful_cildb) {
            // Log error to file
        }
        return ["success" => true, "id" => $insert_id];
    }


    public function deleteAircraft($aircraftID)
    {
        $successful = false;
        #if (self::checkIsAircraftInDB($aircraftID) === true) {
        $stmt = $this->connection->prepare(self::DELETE_SQL);
        if (mysqli_stmt_bind_param($stmt, "i",
            $aircraftID
        )) {

            $successful = mysqli_stmt_execute($stmt) && ($this->connection->affected_rows > 0);

        }


        return $successful;
    }

    /**
     * @return array|bool
     */
    public function getCivilAircraft()
    {
        $resultCivilAirCrafts = false;
        $stmt = $this->connection->prepare(self::SELECT_CIVIL_AIRCRAFT);
        if ($stmt) {

        } else {
            var_dump($this->connection->error);
            exit();
        }
        $successful = mysqli_stmt_execute($stmt);
        if ($successful) {
            $selectResult = mysqli_stmt_get_result($stmt);
            $resultCivilAirCrafts = mysqli_fetch_all($selectResult, MYSQLI_ASSOC);
        }
        if (!$successful) {
            // Log error to file
        }
        return $resultCivilAirCrafts;
    }

    /**
     * @return array|bool|null
     */
    public function getMilitaryAircraft()
    {
        $resultMilitaryAirCrafts = false;
        $stmt = $this->connection->prepare(self::SELECT_MILITARY_AIRCRAFT);
        $successful = mysqli_stmt_execute($stmt);
        if ($successful) {
            $selectResult = mysqli_stmt_get_result($stmt);
            $resultMilitaryAirCrafts = mysqli_fetch_all($selectResult, MYSQLI_ASSOC);
        }
        if (!$successful) {
            // Log error to file
        }
        return $resultMilitaryAirCrafts;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }


}
