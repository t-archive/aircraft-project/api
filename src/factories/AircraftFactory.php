<?php


namespace src\factories;


use src\classes\aircrafts\CivilAircraft;
use src\classes\aircrafts\MilitaryAircraft;
use src\keys\AircraftKeys;

class AircraftFactory
{
    public static function createMilitaryAircraftFromSQL($sqlArray)
    {
        $militaryAircraft = false;

        if (is_array($sqlArray)
            && array_key_exists(AircraftKeys::NAME_DB_KEY, $sqlArray)
            && is_string($sqlArray[AircraftKeys::NAME_DB_KEY])
            && strlen($sqlArray[AircraftKeys::NAME_DB_KEY]) > 0

            && array_key_exists(AircraftKeys::DISTANCE_DB_KEY, $sqlArray)
            && is_int($sqlArray[AircraftKeys::DISTANCE_DB_KEY])

            && array_key_exists(AircraftKeys::SEATCOUNT_DB_KEY, $sqlArray)
            && is_int($sqlArray[AircraftKeys::SEATCOUNT_DB_KEY])

            && array_key_exists(AircraftKeys::MAXSEATCOUNT_DB_KEY, $sqlArray)
            && is_int($sqlArray[AircraftKeys::MAXSEATCOUNT_DB_KEY])

            && array_key_exists(AircraftKeys::LENGTH_DB_KEY, $sqlArray)
            && is_float($sqlArray[AircraftKeys::LENGTH_DB_KEY])

            && array_key_exists(AircraftKeys::WINGSPAN_DB_KEY, $sqlArray)
            && is_float($sqlArray[AircraftKeys::WINGSPAN_DB_KEY])

            && array_key_exists(AircraftKeys::HEIGHT_DB_KEY, $sqlArray)
            && is_float($sqlArray[AircraftKeys::HEIGHT_DB_KEY])

            && array_key_exists(AircraftKeys::CONSUMPTION_DB_KEY, $sqlArray)
            && is_float($sqlArray[AircraftKeys::CONSUMPTION_DB_KEY])
        ) {
            $militaryAircraft = new MilitaryAircraft(
                $sqlArray[AircraftKeys::NAME_DB_KEY],
                intval($sqlArray[AircraftKeys::DISTANCE_DB_KEY]),
                intval($sqlArray[AircraftKeys::SEATCOUNT_DB_KEY]),
                intval($sqlArray[AircraftKeys::MAXSEATCOUNT_DB_KEY]),
                floatval($sqlArray[AircraftKeys::LENGTH_DB_KEY]),
                floatval($sqlArray[AircraftKeys::WINGSPAN_DB_KEY]),
                floatval($sqlArray[AircraftKeys::HEIGHT_DB_KEY]),
                floatval($sqlArray[AircraftKeys::CONSUMPTION_DB_KEY])
            );
            $militaryAircraft->setId($sqlArray[AircraftKeys::ID_DB_KEY]);
            $militaryAircraft->setAirRefueling($sqlArray[AircraftKeys::AIR_REFUELING_DB_KEY]);
            $militaryAircraft->setFlare($sqlArray[AircraftKeys::FLARE_DB_KEY]);

        }

        return $militaryAircraft;
    }

    public static function createCivilAircraftFromSQL($sqlArray)
    {
        $civilAircraft = false;
        if (is_array($sqlArray)
            && array_key_exists(AircraftKeys::NAME_DB_KEY, $sqlArray)
            && is_string($sqlArray[AircraftKeys::NAME_DB_KEY])
            && strlen($sqlArray[AircraftKeys::NAME_DB_KEY]) > 0

            && array_key_exists(AircraftKeys::DISTANCE_DB_KEY, $sqlArray)
            && is_int($sqlArray[AircraftKeys::DISTANCE_DB_KEY])

            && array_key_exists(AircraftKeys::SEATCOUNT_DB_KEY, $sqlArray)
            && is_int($sqlArray[AircraftKeys::SEATCOUNT_DB_KEY])

            && array_key_exists(AircraftKeys::MAXSEATCOUNT_DB_KEY, $sqlArray)
            && is_int($sqlArray[AircraftKeys::MAXSEATCOUNT_DB_KEY])

            && array_key_exists(AircraftKeys::LENGTH_DB_KEY, $sqlArray)
            && is_float($sqlArray[AircraftKeys::LENGTH_DB_KEY])

            && array_key_exists(AircraftKeys::WINGSPAN_DB_KEY, $sqlArray)
            && is_float($sqlArray[AircraftKeys::WINGSPAN_DB_KEY])

            && array_key_exists(AircraftKeys::HEIGHT_DB_KEY, $sqlArray)
            && is_float($sqlArray[AircraftKeys::HEIGHT_DB_KEY])

            && array_key_exists(AircraftKeys::CONSUMPTION_DB_KEY, $sqlArray)
            && is_float($sqlArray[AircraftKeys::CONSUMPTION_DB_KEY])

        ) {
            $civilAircraft = new CivilAircraft(
                $sqlArray[AircraftKeys::NAME_DB_KEY],
                intval($sqlArray[AircraftKeys::DISTANCE_DB_KEY]),
                intval($sqlArray[AircraftKeys::SEATCOUNT_DB_KEY]),
                intval($sqlArray[AircraftKeys::MAXSEATCOUNT_DB_KEY]),
                floatval($sqlArray[AircraftKeys::LENGTH_DB_KEY]),
                floatval($sqlArray[AircraftKeys::WINGSPAN_DB_KEY]),
                floatval($sqlArray[AircraftKeys::HEIGHT_DB_KEY]),
                floatval($sqlArray[AircraftKeys::CONSUMPTION_DB_KEY])
            );
            $civilAircraft->setId($sqlArray[AircraftKeys::ID_DB_KEY]);
            $civilAircraft->setGateLicense($sqlArray[AircraftKeys::GATE_LICENSE_DB_KEY]);
        }

        return $civilAircraft;
    }

    public static function createMilitaryAircraftFromJSON($sqlArray)
    {
        $militaryAircraft = false;
        if (is_array($sqlArray)
            && array_key_exists(AircraftKeys::NAME_JSON_KEY, $sqlArray)
            && is_string($sqlArray[AircraftKeys::NAME_JSON_KEY])
            && strlen($sqlArray[AircraftKeys::NAME_JSON_KEY]) > 0

            && array_key_exists(AircraftKeys::DISTANCE_JSON_KEY, $sqlArray)
            && is_int($sqlArray[AircraftKeys::DISTANCE_JSON_KEY])


            && array_key_exists(AircraftKeys::SEATCOUNT_JSON_KEY, $sqlArray)
            && is_int($sqlArray[AircraftKeys::SEATCOUNT_JSON_KEY])

            && array_key_exists(AircraftKeys::MAXSEATCOUNT_DB_KEY, $sqlArray)
            && is_int($sqlArray[AircraftKeys::MAXSEATCOUNT_JSON_KEY])

            && array_key_exists(AircraftKeys::LENGTH_JSON_KEY, $sqlArray)
            && is_numeric($sqlArray[AircraftKeys::LENGTH_JSON_KEY])

            && array_key_exists(AircraftKeys::WINGSPAN_JSON_KEY, $sqlArray)
            && is_numeric($sqlArray[AircraftKeys::WINGSPAN_JSON_KEY])

            && array_key_exists(AircraftKeys::HEIGHT_JSON_KEY, $sqlArray)
            && is_numeric($sqlArray[AircraftKeys::HEIGHT_JSON_KEY])

            && array_key_exists(AircraftKeys::CONSUMPTION_JSON_KEY, $sqlArray)
            && is_numeric($sqlArray[AircraftKeys::CONSUMPTION_JSON_KEY])

        ) {
            $militaryAircraft = new MilitaryAircraft(
                $sqlArray[AircraftKeys::NAME_JSON_KEY],
                intval($sqlArray[AircraftKeys::DISTANCE_JSON_KEY]),
                intval($sqlArray[AircraftKeys::SEATCOUNT_JSON_KEY]),
                intval($sqlArray[AircraftKeys::MAXSEATCOUNT_JSON_KEY]),
                floatval($sqlArray[AircraftKeys::LENGTH_JSON_KEY]),
                floatval($sqlArray[AircraftKeys::WINGSPAN_JSON_KEY]),
                floatval($sqlArray[AircraftKeys::HEIGHT_JSON_KEY]),
                floatval($sqlArray[AircraftKeys::CONSUMPTION_JSON_KEY])
            );

            $militaryAircraft->setAirRefueling($sqlArray[AircraftKeys::AIR_REFUELING_JSON_KEY]);
            $militaryAircraft->setFlare($sqlArray[AircraftKeys::FLARE_JSON_KEY]);
            if (array_key_exists(AircraftKeys::ID_JSON_KEY, $sqlArray)
                && is_int($sqlArray[AircraftKeys::ID_JSON_KEY])
                && ($sqlArray[AircraftKeys::ID_JSON_KEY]) > 0
            ) {
                $militaryAircraft->setId($sqlArray[AircraftKeys::ID_JSON_KEY]);
            }
        }


        return $militaryAircraft;
    }

    public static function createCivilAircraftFromJSON($sqlArray)
    {
        $civilAircraft = false;
        if (is_array($sqlArray)
            && array_key_exists(AircraftKeys::NAME_JSON_KEY, $sqlArray)
            && is_string($sqlArray[AircraftKeys::NAME_JSON_KEY])
            && strlen($sqlArray[AircraftKeys::NAME_JSON_KEY]) > 0

            && array_key_exists(AircraftKeys::DISTANCE_JSON_KEY, $sqlArray)
            && is_int($sqlArray[AircraftKeys::DISTANCE_JSON_KEY])


            && array_key_exists(AircraftKeys::SEATCOUNT_JSON_KEY, $sqlArray)
            && is_int($sqlArray[AircraftKeys::SEATCOUNT_JSON_KEY])


            && array_key_exists(AircraftKeys::MAXSEATCOUNT_DB_KEY, $sqlArray)
            && is_int($sqlArray[AircraftKeys::MAXSEATCOUNT_JSON_KEY])


            && array_key_exists(AircraftKeys::LENGTH_JSON_KEY, $sqlArray)
            && is_numeric($sqlArray[AircraftKeys::LENGTH_JSON_KEY])

            && array_key_exists(AircraftKeys::WINGSPAN_JSON_KEY, $sqlArray)
            && is_numeric($sqlArray[AircraftKeys::WINGSPAN_JSON_KEY])

            && array_key_exists(AircraftKeys::HEIGHT_JSON_KEY, $sqlArray)
            && is_numeric($sqlArray[AircraftKeys::HEIGHT_JSON_KEY])

            && array_key_exists(AircraftKeys::CONSUMPTION_JSON_KEY, $sqlArray)
            && is_numeric($sqlArray[AircraftKeys::CONSUMPTION_JSON_KEY])

        ) {
            $civilAircraft = new CivilAircraft(
                $sqlArray[AircraftKeys::NAME_JSON_KEY],
                intval($sqlArray[AircraftKeys::DISTANCE_JSON_KEY]),
                intval($sqlArray[AircraftKeys::SEATCOUNT_JSON_KEY]),
                intval($sqlArray[AircraftKeys::MAXSEATCOUNT_JSON_KEY]),
                floatval($sqlArray[AircraftKeys::LENGTH_JSON_KEY]),
                floatval($sqlArray[AircraftKeys::WINGSPAN_JSON_KEY]),
                floatval($sqlArray[AircraftKeys::HEIGHT_JSON_KEY]),
                floatval($sqlArray[AircraftKeys::CONSUMPTION_JSON_KEY])
            );

            $civilAircraft->setGateLicense($sqlArray[AircraftKeys::GATE_LICENSE_JSON_KEY]);
            if (array_key_exists(AircraftKeys::ID_JSON_KEY, $sqlArray)
                && is_int($sqlArray[AircraftKeys::ID_JSON_KEY])
                && ($sqlArray[AircraftKeys::ID_JSON_KEY]) > 0
            ) {
                $civilAircraft->setId($sqlArray[AircraftKeys::ID_JSON_KEY]);
            }
        }

        return $civilAircraft;
    }

}